# jsonschema2py

CLI-tool to generate python dataclasses from a json-schema


## Making Changes & Contributing

This project uses `pre-commit`_, please make sure to install it before making any
changes::

    pip install pre-commit
    cd jsonschema2py
    pre-commit install

It is a good idea to update the hooks to the latest version::

    pre-commit autoupdate

Don't forget to tell your contributors to also install and use [pre-commit](https://pre-commit.com/).

Note
====

This project has been set up using PyScaffold 4.2.1. For details and usage
information on PyScaffold see https://pyscaffold.org/.
