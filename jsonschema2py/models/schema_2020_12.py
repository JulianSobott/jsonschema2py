from typing import Dict, Optional

from pydantic import BaseModel


class RootSchema(BaseModel):
    title: str | None
    type: str
    properties: Dict[str, "RootSchema"] | None
    items: Optional["RootSchema"]
