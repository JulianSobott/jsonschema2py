import json
from typing import TypeVar

Target = TypeVar("Target")


def from_string(src: str, target: type) -> Target:
    data = json.loads(src)
    return target(**data)
