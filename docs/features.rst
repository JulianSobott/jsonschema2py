Features
========

==============================  =========  =========
\                               dataclass  BaseModel
==============================  =========  =========
field: type                      ❌          ❌
field: properties                ❌          ❌
field: items                     ❌          ❌
field: $ref                      ❌          ❌
field: description               ❌          ❌
yaml support                     ❌          ❌
==============================  =========  =========
