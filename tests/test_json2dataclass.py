import json
from dataclasses import dataclass

from jsonschema2py.json2dataclass import from_string


def test_flat_json_to_dataclass():
    @dataclass
    class X:
        w: bool
        x: int
        y: str
        z: float

    src = {"w": True, "x": 10, "y": "Hello World", "z": 5.3}
    data = json.dumps(src)
    parsed = from_string(data, X)
    assert parsed.w is True
    assert parsed.x == 10
    assert parsed.y == "Hello World"
    assert parsed.z == 5.3
